package fr.marmat.jersey;


/**
 * This Object is a Data Transfer Object.
 * It is used to transfer data from the body of the request to the real {@link Account}Account Object
 * It's just a JavaBean with getters and setters. 
 *
 */
public class AccountDto {
	private long id;	//account ID
	private float balance; //Balance For this Account
	private String type;
	private int pid;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	
	
	
	

}
