package fr.marmat.jersey;

import org.skife.jdbi.v2.DBI;
import org.sqlite.SQLiteDataSource;

import javax.inject.Singleton;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This Class is a Singleton.
 * It's the Database Factory, creating it and creating the Database Interface to dialog with it.
 */
@Singleton
public class BDDFactory {
    private static DBI dbi = null;
    
    /**
     * Get the database Interface and if it does not already exist, initialise it.
     * @return DBI Databse Interface ready to be used for SQL Query and Updates
     */
    public static DBI getDbi() {
        if(dbi == null) {
            SQLiteDataSource ds = new SQLiteDataSource();
            ds.setUrl("jdbc:sqlite:" + System.getProperty("user.dir") + System.getProperty("file.separator") + "data.db");
            dbi = new DBI(ds);
        }
        return dbi;
    }
    /**
     * Check if a Table exists in the database
     * @param tableName Name of the table
     * @return true if the table is in the database, false if it isn't
     * @throws SQLException If it there is an issue with the database.
     */
    static boolean tableExist(String tableName) throws SQLException {
        DatabaseMetaData dbm = getDbi().open().getConnection().getMetaData();
        ResultSet tables = dbm.getTables(null, null, tableName, null);
        boolean exist = tables.next();
        tables.close();
        return exist;
    }
}
