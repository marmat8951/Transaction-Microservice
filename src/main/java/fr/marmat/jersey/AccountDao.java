package fr.marmat.jersey;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapperFactory;
import org.skife.jdbi.v2.tweak.BeanMapperFactory;

/**
 * This is the Data Access Object. It is used to make SQL request to the database.
 * It's the Data Acces Layer
 */
public interface AccountDao {
	
	@SqlUpdate("create table account (id INTEGER PRIMARY KEY, balance FLOAT, type varchar(12) NOT NULL, pid INTEGER NOT NULL)")
    void createAccountTable();
	
	@SqlUpdate("drop table if exists account")
    void dropAccountTable();
 
	@SqlUpdate("insert into account (id, balance, type, pid) values (:id,:balance,:type,:pid)")
	public int insert(@BindBean() Account acc);
	
	@SqlUpdate("UPDATE account set balance=:balance, type=:type pid=:pid where id=:id")
	public int update(@BindBean() Account acc);
	
	@SqlUpdate("DELETE from account where id=:id")
	public int delete(@Bind("id") long id);
	
	@SqlUpdate("DELETE from account where pid=:pid")
	public int deleteUsingPid(@Bind("pid") int pid);
	
	@SqlQuery("select * from account order by id ASC")
	@RegisterMapperFactory(BeanMapperFactory.class)
	public List<Account> all();
	
	@SqlQuery("select * from account order by balance ASC")
	@RegisterMapperFactory(BeanMapperFactory.class)
	public List<Account> allOrderByBalanceASC();
	
	@SqlQuery("select * from account WHERE pid=:pid")
	@RegisterMapperFactory(BeanMapperFactory.class)
	public List<Account> allAccountWithPid(@Bind("pid") int pid);
	
	@SqlQuery("select * from account where id=:id")
	@RegisterMapperFactory(BeanMapperFactory.class)
	public Account findAccountById(@Bind("id") long id);
	
	@SqlQuery("select sum(balance) from account")
	public double sum();
}
