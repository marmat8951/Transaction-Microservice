package fr.marmat.jersey;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "accountType")
@XmlEnum
@XmlRootElement
/**
 * This enum is here to replace the actual String of the accounts.
 * It serves to restrict authorised account types and to (possibly)
 * provide methods as utils on accounts, like the prime rate
 *
 */
public enum AccountType {
	
	@XmlEnumValue(value = "Checking")CHECKING,
	@XmlEnumValue(value = "Saving")SAVING,
	@XmlEnumValue(value = "Other")OTHER;
	
	public String toString() {
		if(this.equals(SAVING)) {
			return "Saving";
		}else if(this.equals(CHECKING)) {
			return "Checking";
		}else return "Other";
	}
	
	public static AccountType parseAccountType(String string) {
		if(string.equalsIgnoreCase("Checking")) {
			return CHECKING;
		}else if(string.equalsIgnoreCase("Saving")) {
			return SAVING;
		}else return OTHER;
	}
	
}
