package fr.marmat.jersey;

import static fr.marmat.jersey.BDDFactory.getDbi;
import static fr.marmat.jersey.BDDFactory.tableExist;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *  Exposed at /account
 *  this is the most front-end Layer exposing ressources.
 *  
 */
@Path("account")
public class AccountRessource {

	private static AccountDao dao = getDbi().open(AccountDao.class);

	/**
	 * Create a new account and the accountTable in the database if it isn't yet present
	 * @throws SQLException if the is an error in the SQL database
	 */
	public AccountRessource() throws SQLException {
		if (!tableExist("account")) {
			dao.createAccountTable();
		}
	}


	/**
	 * Method handling HTTP GET requests on /api/account. The returned object will be sent
	 * to the client as "application/json" media type.
	 *
	 * @return String that will be returned as a application/json response.
	 */
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<AccountDto> getAllAccounts() {
		List<Account> accounts = dao.allOrderByBalanceASC();
	    return accounts.stream().map(Account::convertToDto).collect(Collectors.toList());
	}
	
	/**
	 * Method handling HTTP GET requests on /api/account/{id}. 
	 * {id} is an integer representing the account ID requested
	 * The returned object will be sent to the client as "application/json" media type.
	 * @throws NotFoundException if the Id isn't a correct id
	 * @return String as a application/json representation of the Account.
	 */
    @GET @Path("/{id:[0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public AccountDto getAccountWithId(@PathParam("id") long id) {
    	
    	Account a = dao.findAccountById(id);
    	if(a==null) {
    	throw new NotFoundException();
    	}
    	return a.convertToDto();
    }
	
    /**
     * Method handling HTTP GET requests on /api/account/sum.
     * @return a text/PlainText Representing the total sum
     */
	@GET @Path("/sum")
	@Produces(MediaType.TEXT_PLAIN)
	public String getSum() {
		double sum = dao.sum();
		NumberFormat nf = NumberFormat.getCurrencyInstance();
		return nf.format(sum);
	}

	 
    /**
     * Method handling HTTP GET request on /api/account/test
     * It creates an account and return It so that it can be used for
     * making tests of insertion and getting data
     * 
     * @return String representing the test account
     */
	@GET @Path("/test")
	@Produces(MediaType.APPLICATION_JSON)
	public Account getTestAccount() {
		Account a = new Account();
		a.setId(new Long("8745625478"));
		a.setBalance((float) 574.5);
		a.setType(AccountType.CHECKING);
		a.setPid(1);
		return a;
	}
	/**
	 * Method handling HTTP GET requests on /api/account/pid/{pid}
	 * @param pid Integer representing the PersonId we want to use to get all the accounts corresponding 
	 * @return a String representing an array of accounts.
	 */
	@GET @Path("/pid/{pid: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<AccountDto> getAllAccountswithPid(@PathParam("pid") int pid) {
		List<Account> accounts = dao.allAccountWithPid(pid);
	    return accounts.stream().map(Account::convertToDto).collect(Collectors.toList());
	}
	
	/**
	 * Method handling HTTP POST Requests on /api/account
	 * It is used to create a new Account with the parameters
	 * @param dto Data Transfer Object Binded with the body values
	 * @return 201 Created HTTP response
	 * @throws URISyntaxException if there is an exception generating the new Account URI
	 */
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createNewAccount(AccountDto dto) throws URISyntaxException {
		Account a = new Account();
		a.setBalance(dto.getBalance());
		a.setId(dto.getId());
		a.setType(dto.getType());
		a.setPid(dto.getPid());
		dao.insert(a);
		return Response.created(new URI(Main.BASE_URI+"account/"+a.getId())).build();
	}
	
	
	@PUT @Path("/{id: [0-9]+}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Account putAccount(@PathParam("id") long id, AccountDto dto) {
		if(dto == null | id != dto.getId()) {
			throw new BadRequestException();
		}
		Account a = dao.findAccountById(id);
		a.setPid(dto.getPid());
		a.setBalance(dto.getBalance());
		a.setType(dto.getType());
		int res = dao.update(a);
		return a;
	}
	
	
	/**
	 * Method handling HTTP DELETE request on /api/account/{id} to delete account
	 * @param id integer corresponding to the Account ID we want to delete
	 * @throws BadRequestException If the account ID is not correct
	 * @throws NotFoundException If the account ID doesn't exists
	 * @return HTTP 201 No Content if everything went fine. Otherwise it throws an exception.
	 */
	@DELETE @Path("/{id: [0-9]+}")
	public Response deleteAccount(@PathParam("id") long id) {
		int val = dao.delete(id);
		if(val<0) {
			throw new  BadRequestException();
		}else if(val == 0) {
			throw new NotFoundException();
		}else {
			return Response.noContent().build();
		}
	}
	
	/**
	 * Method handeling HTTP DELETE request on /api/account/pid/{pid}
	 * @param pid Person ID we are deleting the account(s)
	 * @return HTTP 201 No Content if everything went fine. Otherwise it throws an exception.
	 * @throws BadRequestException if the Pid is incorrect
	 * @throws NotFoundException if there are no accounts for this pid.
	 */
	@DELETE @Path("/pid/{pid: [0-9]+}")
	public Response deleteAccountsOfPid(@PathParam("pid") int pid) {
		int val = dao.deleteUsingPid(pid);
		if(val<0) {
			throw new  BadRequestException();
		}else if(val == 0) {
			throw new NotFoundException();
		}else {
			return Response.noContent().build();
		}
		
	}
	
	
	
}
