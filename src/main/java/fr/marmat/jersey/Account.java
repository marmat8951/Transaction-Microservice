package fr.marmat.jersey;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Account {
	private long id;	//account ID
	private float balance; //Balance For this Account
	private String type;
	private int pid;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public float getBalance() {
		return balance;
	}
	public void setBalance(float balance) {
		this.balance = balance;
	}
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.setType(AccountType.parseAccountType(type));
	}
	
	public void setType(AccountType type) {
		this.type = type.toString();
	}
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public void initFromDto(AccountDto dto) {
		this.setId(dto.getId());
		this.setBalance(dto.getBalance());
		this.setType(dto.getType());
		this.setPid(dto.getPid());
	}

	public AccountDto convertToDto() {
		AccountDto dto = new AccountDto();
		dto.setBalance(getBalance());
		dto.setId(getId());
		dto.setType(type.toString());
		dto.setPid(getPid());
		return dto;
	}





}
