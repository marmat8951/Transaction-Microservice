package fr.marmat.jersey;

import static fr.marmat.jersey.BDDFactory.getDbi;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.Consumes;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@Path("withdraw")
public class WithdrawRessource {
	
	private static AccountDao dao = getDbi().open(AccountDao.class);

	public WithdrawRessource() {
		
	}
	
	/**
	Called when a POST is used on api/withdraw/{id}
    the id is an long representing the account ID we are withdrawing.
    This Consumes Plain-text Representing the amount we want to withdraw from the account formatted as float.
    @throws NumberFormatException If number is <= 0 or if the number couldn't be parsed
    @throws NotFoundException If there is no account with this id
    @param id Long int the path corresponding to the Id of the account we are deposing at.
    @param value String corresponding to the body content
     */
	@POST @Path("/{id:[0-9]+}")
	@Consumes(MediaType.TEXT_PLAIN)
	public int depositToThisAccount( @PathParam("id") Long id,String value){
		Account res=dao.findAccountById(id);
		value = value.replaceAll(",", ".");
		if(res == null) {
			throw new NotFoundException();
		}
		float f;
		try {
			f = Float.parseFloat(value);
			if(f<=0) {
				throw new NumberFormatException();
			}
		}catch(NumberFormatException | NullPointerException ne) {
			throw new BadRequestException();
		}
		res.setBalance(res.getBalance()-f);
		return dao.update(res);
		
	}

}
