package fr.marmat.jersey;

import static fr.marmat.jersey.BDDFactory.getDbi;
import static org.junit.Assert.assertEquals;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AccountTest {

    private HttpServer server;
    private WebTarget target;
    private Client c;
    private static AccountDao dao = getDbi().open(AccountDao.class);

    @Before
    public void setUp() throws Exception {
        // start the server
        server = Main.startServer();
        // create the client
        c = ClientBuilder.newClient();

        target = c.target(Main.BASE_URI);
        
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
       
        
    }

    /**
     * Test to see if the testRessource works
     */
    
    @Test
    public void testGetTestAccount() {
        String responseMsg = target.path("account/test").request().get(String.class);
        assertEquals("{\"balance\":574.5,\"id\":8745625478,\"pid\":1,\"type\":\"Checking\"}", responseMsg);
    }
    
    
    @Test
    public void testPostAccount() {
    	Client client = c;
    	dao.delete(7355608);
        WebTarget target = client.target("http://0.0.0.0:8080/api/account");
        String stringData = "{\"balance\":1337.4,\"id\":7355608,\"type\":\"Checking\",\"pid\":1}";
        Entity<String> data = Entity.entity(stringData, MediaType.APPLICATION_JSON_TYPE);
        Response result = target.request(MediaType.APPLICATION_JSON).post(data);
        int res = result.getStatus();
        assertEquals(201, res);
        dao.delete(7355608);
        
    }
    
    
}
